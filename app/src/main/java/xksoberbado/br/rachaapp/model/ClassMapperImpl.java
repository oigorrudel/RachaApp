package xksoberbado.br.rachaapp.model;

import java.util.Arrays;
import java.util.HashSet;

import xksoberbado.br.rachaapp.arq.dao.IClassMapper;

public class ClassMapperImpl implements IClassMapper {
    @Override
    public HashSet<Class> getEntities() {
        return new HashSet<Class>(
                Arrays.asList(Purchase.class,
                        PurchaseItem.class,
                        Participant.class,
                        PurchaseItemParticipant.class));
    }
}
