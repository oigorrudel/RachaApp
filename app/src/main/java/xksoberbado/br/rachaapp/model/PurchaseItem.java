package xksoberbado.br.rachaapp.model;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.field.ForeignCollectionField;
import com.j256.ormlite.table.DatabaseTable;

import java.math.BigDecimal;
import java.util.Collection;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import xksoberbado.br.rachaapp.arq.model.Entity;

/**
 * Created by igor.rudel on 03/07/2018.
 */
@NoArgsConstructor
@Getter
@Setter
@DatabaseTable(tableName = "purchase_items")
public class PurchaseItem extends Entity {

    @DatabaseField(canBeNull = false)
    private String name;

    @DatabaseField(canBeNull = false)
    private Integer quantity;

    @DatabaseField(canBeNull = false)
    private BigDecimal price;

    @DatabaseField(canBeNull = false, foreign = true, columnName = "purchase_id", foreignAutoRefresh = true)
    private Purchase purchase;

    @ForeignCollectionField(eager = true)
    private Collection<PurchaseItemParticipant> participants;

    public PurchaseItem(String name, Integer quantity, BigDecimal price, Purchase purchase) {
        this.name = name;
        this.quantity = quantity;
        this.price = price;
        this.purchase = purchase;
    }

    public BigDecimal getTotal(){
        return price.multiply(new BigDecimal(quantity));
    }
}
