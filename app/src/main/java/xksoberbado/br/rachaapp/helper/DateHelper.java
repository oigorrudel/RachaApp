package xksoberbado.br.rachaapp.helper;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by igor.rudel on 05/07/2018.
 */

public class DateHelper {

    private static SimpleDateFormat sdfWithTime = new SimpleDateFormat("HH:mm dd/MM/yyyy");
    private static SimpleDateFormat sdfWithoutTime = new SimpleDateFormat("dd/MM/yyyy");

    private DateHelper() {
    }

    public static String formatDateWithTime(Date date){
        return sdfWithTime.format(date);
    }

    public static String formatDateWithoutTime(Date date){
        return sdfWithoutTime.format(date);
    }
}
