package xksoberbado.br.rachaapp.helper;

import java.math.BigDecimal;

/**
 * Created by igor.rudel on 05/07/2018.
 */

public class MoneyHelper {

    private MoneyHelper() {
    }

    public static BigDecimal setTwoDecimalPlaces(BigDecimal price){
        return price.setScale(2, BigDecimal.ROUND_HALF_UP);
    }
}
