package xksoberbado.br.rachaapp.calculator;

import java.math.BigDecimal;
import java.math.MathContext;

import xksoberbado.br.rachaapp.model.Participant;
import xksoberbado.br.rachaapp.model.Purchase;
import xksoberbado.br.rachaapp.model.PurchaseItem;
import xksoberbado.br.rachaapp.model.PurchaseItemParticipant;

public class CalculatorSimple extends AbstractCalculator {

    public CalculatorSimple(Purchase purchase) {
        super(purchase);
        setRest();
    }

    @Override
    public void execute() {
        for(Participant p : purchase.getParticipants()){
            results.add(new Result(p, new BigDecimal(getTotalParticipant(p).doubleValue()).setScale(2, BigDecimal.ROUND_DOWN)));
        }
    }

    public void setRest() {
        BigDecimal purchaseTotal = purchase.getTotal();
        BigDecimal totalResults = getTotalResults();

        BigDecimal diff = purchaseTotal.subtract(totalResults);
        if(diff.doubleValue() > 0)
            results.add(new Result(new Participant("Resto", null), diff));

    }

    private BigDecimal getTotalResults(){
        BigDecimal total = new BigDecimal(0);
        for(Result result : results)
            total = total.add(result.getTotal());

        return total;
    }

    private BigDecimal getTotalParticipant(Participant participant){
        BigDecimal total = new BigDecimal(0);
        for(PurchaseItem item : purchase.getItems()){
            BigDecimal qttParticipants = new BigDecimal(purchase.getParticipants().size());
            if(!item.getParticipants().isEmpty()){
                BigDecimal qttItemParticipants = new BigDecimal(item.getParticipants().size());
                for(PurchaseItemParticipant pip : item.getParticipants()){
                    if(pip.getParticipant().getId().equals(participant.getId())){
                        total = total.add(item.getTotal().divide(qttItemParticipants, MathContext.DECIMAL64));
                    }
                }
            } else {
                total = total.add(item.getTotal().divide(qttParticipants, MathContext.DECIMAL64));
            }

        }
        return total;
    }
}
