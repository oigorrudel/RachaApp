package xksoberbado.br.rachaapp.view.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.Collection;

import xksoberbado.br.rachaapp.R;
import xksoberbado.br.rachaapp.arq.view.IYesNo;
import xksoberbado.br.rachaapp.arq.view.YesNoDialog;
import xksoberbado.br.rachaapp.repository.PurchaseItemParticipantDao;
import xksoberbado.br.rachaapp.view.dialog.ParticipantsDialog;
import xksoberbado.br.rachaapp.helper.DialogFragmentHelper;
import xksoberbado.br.rachaapp.helper.MoneyHelper;
import xksoberbado.br.rachaapp.model.PurchaseItem;
import xksoberbado.br.rachaapp.model.PurchaseItemParticipant;
import xksoberbado.br.rachaapp.repository.PurchaseItemDao;
import xksoberbado.br.rachaapp.view.form.PurchaseItemDialog;

import static xksoberbado.br.rachaapp.helper.MoneyHelper.*;

/**
 * Created by Igor on 03/07/2018.
 */

public class PurchaseItemRvAdapter extends RecyclerView.Adapter<PurchaseItemRvAdapter.ViewLine> {

    private ArrayList<PurchaseItem> items;

    public PurchaseItemRvAdapter(ArrayList<PurchaseItem> items) {
        this.items = items;
    }

    @NonNull
    @Override
    public ViewLine onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.purchase_item_line, parent, false);
        return new ViewLine(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewLine holder, final int position) {
        final PurchaseItem item = items.get(position);
        int qttItemParticipants = item.getParticipants().size();
        int qttPurchaseParticipants = item.getPurchase().getParticipants().size();

        holder.tv.setText(position+1 +" - "+ item.getName());
        holder.tvTotal.setText(item.getQuantity() +" x "+ setTwoDecimalPlaces(item.getPrice()) +" = R$ "+ setTwoDecimalPlaces(item.getTotal()));

        holder.ivMarker.setVisibility((qttItemParticipants == 0 || qttItemParticipants == qttPurchaseParticipants) && qttPurchaseParticipants > 0 ? View.VISIBLE : View.INVISIBLE);
        holder.ivMarker.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(holder.getContext(), R.string.info_marker, Toast.LENGTH_SHORT).show();
            }
        });


        holder.ibParticipants.setVisibility(qttPurchaseParticipants > 0 ? View.VISIBLE : View.GONE);
        holder.ibParticipants.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DialogFragmentHelper.showFragmentDialog(new ParticipantsDialog(item), "participants_dialog");
            }
        });

        holder.ibEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DialogFragmentHelper.showFragmentDialog(new PurchaseItemDialog(item), "purchase_item_dialog_edit");
            }
        });

        holder.ibDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DialogFragmentHelper.showFragmentDialog(new YesNoDialog(R.string.purchase_item_delete_ask, delete(item, position)), "yes_no_purchase_dialog");
            }
        });
    }

    @Override
    public int getItemCount() {
        return items.size();
    }


    public class ViewLine extends RecyclerView.ViewHolder{

        public TextView tv;
        public TextView tvTotal;
        public ImageView ivMarker;
        public ImageButton ibParticipants;
        public ImageButton ibEdit;
        public ImageButton ibDelete;

        public ViewLine(@NonNull View itemView) {
            super(itemView);
            this.tv = itemView.findViewById(R.id.tv_default);
            this.tvTotal = itemView.findViewById(R.id.tv_total);
            this.ivMarker = itemView.findViewById(R.id.iv_marker);
            this.ibParticipants = itemView.findViewById(R.id.ib_participants);
            this.ibEdit = itemView.findViewById(R.id.ib_edit);
            this.ibDelete = itemView.findViewById(R.id.ib_delete);
        }

        public Context getContext(){
            return this.itemView.getContext();
        }
    }

    private IYesNo delete(final PurchaseItem item, final int position){
        return new IYesNo() {
            @Override
            public void yes() {
                items.remove(position);
                new PurchaseItemParticipantDao().deleteByItem(item);
                new PurchaseItemDao().delete(item, R.string.purchase_item_deleted);
                notifyItemRemoved(position);
            }

            @Override
            public boolean onlyDismiss() {
                return true;
            }

            @Override
            public void no() {

            }
        };
    }
}
