package xksoberbado.br.rachaapp.view.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import xksoberbado.br.rachaapp.R;
import xksoberbado.br.rachaapp.arq.App;
import xksoberbado.br.rachaapp.model.Participant;
import xksoberbado.br.rachaapp.model.PurchaseItem;
import xksoberbado.br.rachaapp.model.PurchaseItemParticipant;
import xksoberbado.br.rachaapp.repository.PurchaseItemDao;
import xksoberbado.br.rachaapp.repository.PurchaseItemParticipantDao;
import xksoberbado.br.rachaapp.view.fragment.PurchaseItemsFragment;

public class SelectParticipantRvAdapter extends RecyclerView.Adapter<SelectParticipantRvAdapter.ViewLine> {

    private ArrayList<Participant> purchaseParticipants;
    private PurchaseItem item;

    public SelectParticipantRvAdapter(ArrayList<Participant> purchaseParticipants, PurchaseItem item) {
        this.purchaseParticipants = purchaseParticipants;
        this.item = item;
    }

    @NonNull
    @Override
    public ViewLine onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.select_participant_line, parent, false);
        return new ViewLine(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewLine holder, int position) {
        final Participant p = purchaseParticipants.get(position);

        holder.tvParticipant.setText(p.getName());

        final boolean containsParticipant = containsParticipant(p);
        holder.ibAddRemove.setImageResource(!containsParticipant ? R.mipmap.add_box : R.mipmap.remove_circle);
        holder.ibAddRemove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PurchaseItemParticipantDao dao = new PurchaseItemParticipantDao();
                if(!containsParticipant){
                    dao.create(new PurchaseItemParticipant(item, p));
                } else {
                    dao.delete(getPurchaseItemParticipant(p));
                }
                notifyDataSetChanged();
                App.sendBroadcast(PurchaseItemsFragment.REFRESH);
            }
        });
    }

    private PurchaseItemParticipant getPurchaseItemParticipant(Participant participant){
        if(!item.getParticipants().isEmpty()){
            for(PurchaseItemParticipant pip : item.getParticipants()){
                if(pip.getParticipant().getId().equals(participant.getId()))
                    return pip;
            }
        }
        return null;
    }

    private boolean containsParticipant(Participant participant){
        new PurchaseItemDao().refresh(item);
        return getPurchaseItemParticipant(participant) != null;
    }

    @Override
    public int getItemCount() {
        return purchaseParticipants.size();
    }

    public class ViewLine extends RecyclerView.ViewHolder{

        public TextView tvParticipant;
        public ImageButton ibAddRemove;

        public ViewLine(@NonNull View itemView) {
            super(itemView);
            this.tvParticipant = itemView.findViewById(R.id.tv_default);
            this.ibAddRemove = itemView.findViewById(R.id.ib_add_remove);
        }
    }

}
