package xksoberbado.br.rachaapp.view.dialog;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import xksoberbado.br.rachaapp.R;
import xksoberbado.br.rachaapp.helper.ReceiptHelper;
import xksoberbado.br.rachaapp.model.Purchase;

public class ReceiptDialog extends DialogFragment {

    private Purchase purchase;

    private ReceiptDialog() {
    }

    public ReceiptDialog(Purchase purchase) {
        this.purchase = purchase;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View fragment = inflater.inflate(R.layout.receipt, null);
        ImageView ivReceipt = fragment.findViewById(R.id.iv_receipt);
        ivReceipt.setImageBitmap(ReceiptHelper.generate(purchase));
        return fragment;
    }
}
