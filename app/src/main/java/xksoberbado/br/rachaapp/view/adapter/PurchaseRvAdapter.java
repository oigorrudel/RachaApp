package xksoberbado.br.rachaapp.view.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.PopupMenu;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import xksoberbado.br.rachaapp.R;
import xksoberbado.br.rachaapp.arq.helper.PopupMenuHelper;
import xksoberbado.br.rachaapp.arq.view.IYesNo;
import xksoberbado.br.rachaapp.arq.view.YesNoDialog;
import xksoberbado.br.rachaapp.view.form.PurchaseDialog;
import xksoberbado.br.rachaapp.view.fragment.DivisionFragment;
import xksoberbado.br.rachaapp.helper.DialogFragmentHelper;
import xksoberbado.br.rachaapp.repository.PurchaseDao;
import xksoberbado.br.rachaapp.view.dialog.ReceiptDialog;
import xksoberbado.br.rachaapp.view.fragment.ParticipantsFragment;
import xksoberbado.br.rachaapp.helper.DateHelper;
import xksoberbado.br.rachaapp.helper.FragmentHelper;
import xksoberbado.br.rachaapp.model.Purchase;
import xksoberbado.br.rachaapp.view.fragment.PurchaseItemsFragment;

/**
 * Created by igor.rudel on 03/07/2018.
 */

public class PurchaseRvAdapter extends RecyclerView.Adapter<PurchaseRvAdapter.ViewLine> {

    private ArrayList<Purchase> purchases;

    private PurchaseRvAdapter() {
    }

    public PurchaseRvAdapter(ArrayList<Purchase> purchases) {
        this.purchases = purchases;
    }

    @NonNull
    @Override
    public ViewLine onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.purchase_line, parent, false);
        return new ViewLine(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewLine holder, final int position) {
        final Purchase purchase = purchases.get(position);

        holder.tv.setText(purchase.getName() + (purchase.getPercentage() == null ? "" : " - "+ purchase.getPercentage() +"%"));

        holder.tvDate.setText(holder.getString(R.string.create_date) +": "+ DateHelper.formatDateWithTime(purchase.getDate()));


        holder.ibMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PopupMenu popupMenu = new PopupMenu(holder.getContext(), holder.ibMore);
                popupMenu.getMenuInflater().inflate(R.menu.more_purchase, popupMenu.getMenu());

                boolean hasParticipantsAndItems = purchase.getParticipants().size() > 0 && purchase.getItems().size() > 0;

                MenuItem opDivide = popupMenu.getMenu().findItem(R.id.op_divide);
                opDivide.setVisible(hasParticipantsAndItems);

//                MenuItem opReceipt = popupMenu.getMenu().findItem(R.id.op_receipt);
//                opReceipt.setVisible(hasParticipantsAndItems);

                PopupMenuHelper.showIcons(popupMenu);
                popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem menuItem) {
                        switch (menuItem.getItemId()){
                            case R.id.op_edit:
                                DialogFragmentHelper.showFragmentDialog(new PurchaseDialog(purchase), "purchase_dialog_edit");
                                break;
                            case R.id.op_see_participants:
                                FragmentHelper.replaceFragment(new ParticipantsFragment(purchase));
                                break;
                            case R.id.op_see_items:
                                FragmentHelper.replaceFragment(new PurchaseItemsFragment(purchase));
                                break;
                            case R.id.op_divide:
                                FragmentHelper.replaceFragment(new DivisionFragment(purchase));
                                break;
//                            case R.id.op_receipt:
//                                DialogFragmentHelper.showFragmentDialog(new ReceiptDialog(purchase), "receipt_dialog");
//                                break;
                            case R.id.op_delete:
                                DialogFragmentHelper.showFragmentDialog(new YesNoDialog(R.string.purchase_delete_ask, delete(purchase, position)), "yes_no_purchase");
                                break;
                        }
                        return false;
                    }
                });
                popupMenu.show();
            }
        });
    }

    @Override
    public int getItemCount() {
        return purchases.size();
    }

    public class ViewLine extends RecyclerView.ViewHolder{

        public TextView tv;
        public TextView tvDate;
        public ImageButton ibMore;

        public ViewLine(@NonNull View itemView) {
            super(itemView);
            this.tv = itemView.findViewById(R.id.tv_default);
            this.tvDate = itemView.findViewById(R.id.tv_purchase_date);
            this.ibMore = itemView.findViewById(R.id.ib_more);
        }

        public Context getContext(){
            return this.itemView.getContext();
        }

        public String getString(int res){
            return getContext().getString(res);
        }
    }

    private IYesNo delete(final Purchase purchase, final int position){
        return new IYesNo() {
            @Override
            public void yes() {
                purchases.remove(position);
                new PurchaseDao().delete(purchase, R.string.purchase_deleted);
                notifyItemRemoved(position);
            }

            @Override
            public boolean onlyDismiss() {
                return true;
            }

            @Override
            public void no() {

            }
        };
    }
}
