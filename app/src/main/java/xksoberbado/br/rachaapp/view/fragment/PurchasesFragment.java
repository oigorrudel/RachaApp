package xksoberbado.br.rachaapp.view.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import xksoberbado.br.rachaapp.R;
import xksoberbado.br.rachaapp.arq.App;
import xksoberbado.br.rachaapp.repository.PurchaseDao;
import xksoberbado.br.rachaapp.view.adapter.PurchaseRvAdapter;
import xksoberbado.br.rachaapp.model.Purchase;

/**
 * Created by igor.rudel on 03/07/2018.
 */

public class PurchasesFragment extends Fragment {

    private RecyclerView rv;

    public PurchasesFragment() {
        setHasOptionsMenu(true);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        App.getActivity().getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        getActivity().setTitle(R.string.puchases);
        View fragment = inflater.inflate(R.layout.rv_default, null);
        rv = fragment.findViewById(R.id.rv_default);
        rv.setLayoutManager(new LinearLayoutManager(getContext()));
        ArrayList<Purchase> purchases = new ArrayList<>(new PurchaseDao().getAll());
        if(purchases != null && purchases.size() > 0){
            rv.setAdapter(new PurchaseRvAdapter(purchases));
        }

        return fragment;
    }
}
