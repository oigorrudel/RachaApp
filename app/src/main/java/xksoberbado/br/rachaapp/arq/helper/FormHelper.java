package xksoberbado.br.rachaapp.arq.helper;

import android.widget.EditText;

import xksoberbado.br.rachaapp.arq.App;
import xksoberbado.br.rachaapp.R;

public class FormHelper {

    private FormHelper() {
    }

    public static boolean validateString(String str){
        return str != null && !str.trim().isEmpty();
    }

    public static boolean validateEt(EditText et){
        if(et.getText().toString().trim().isEmpty()){
            setMessageError(et);
            return false;
        }

        return true;
    }

    public static boolean validateEts(EditText... ets){
        for(EditText et : ets){
            if(et.getText().toString().trim().isEmpty()){
                setMessageError(et);
                return false;
            }
        }
        return true;
    }

    private static void setMessageError(EditText et){
        et.setError(App.getContext().getString(R.string.fill));
    }

}
