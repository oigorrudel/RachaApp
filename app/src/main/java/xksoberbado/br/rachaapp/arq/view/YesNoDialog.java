package xksoberbado.br.rachaapp.arq.view;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;

import xksoberbado.br.rachaapp.R;
import xksoberbado.br.rachaapp.arq.App;

public class YesNoDialog extends DialogFragment {

    private IYesNo iYesNo;
    private int message;
    private String messageString;

    private YesNoDialog() {
    }

    public YesNoDialog(int message, IYesNo iYesNo) {
        this.message = message;
        this.iYesNo = iYesNo;
    }

    public YesNoDialog(String messageString, IYesNo iYesNo) {
        this.messageString = messageString;
        this.iYesNo = iYesNo;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(App.getActivity());

        if(messageString == null)
            builder.setMessage(message);
        else
            builder.setMessage(messageString);

        builder.setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                if(iYesNo.onlyDismiss())
                    dismiss();
                else
                    iYesNo.no();
            }
        });

        builder.setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                iYesNo.yes();
                dismiss();
            }
        });

        return builder.create();
    }
}
