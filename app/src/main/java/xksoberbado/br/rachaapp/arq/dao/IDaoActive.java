package xksoberbado.br.rachaapp.arq.dao;

import java.util.Collection;

import xksoberbado.br.rachaapp.arq.model.ActiveEntity;

public interface IDaoActive<E> extends IDao {

    void enable(ActiveEntity e);

    void enable(ActiveEntity e, Integer msgId);

    void disable(ActiveEntity e);

    void disable(ActiveEntity e, Integer msgId);

    Collection<E> getAllEnabled();

    Collection<E> getAllDisabled();

}
