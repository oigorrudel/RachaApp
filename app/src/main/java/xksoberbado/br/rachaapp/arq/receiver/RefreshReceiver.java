package xksoberbado.br.rachaapp.arq.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import xksoberbado.br.rachaapp.arq.view.IRefresh;

public class RefreshReceiver extends BroadcastReceiver {

    private IRefresh ref;

    public RefreshReceiver(IRefresh ref) {
        this.ref = ref;
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        ref.refresh();
    }
}
