package xksoberbado.br.rachaapp.arq.dao;

import java.util.Collection;

public interface IDao<E> {

    void create(E e);

    void update(E e);

    void delete(E e);

    void create(E e, Integer idMsg);

    void update(E e, Integer idMsg);

    void delete(E e, Integer idMsg);

    void deleteAll();

    Collection<E> getAll();

    E findById(Long id);

    Collection<E> getAllIn(Iterable<Long> ids);

    long countOf();

    void refresh(E e);
}
